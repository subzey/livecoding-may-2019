export async function initPhysics() {
  const res = await WebAssembly.instantiateStreaming(
    fetch('physics.wasm'),
    { Math }
  );
  const wasmExports = res.instance.exports;

  let f32a = new Float32Array(0);
  function getData() {
    if (wasmExports.mem.buffer !== f32a.buffer) {
      f32a = new Float32Array(wasmExports.mem.buffer);
    }
    return f32a;
  }

  return {
    getData,
    tick: wasmExports.tick,
    fire: wasmExports.fire,
  }
}
