(module
  (func $random (import "Math" "random") (result f32))
  (func $sin (import "Math" "sin") (param f32) (result f32))
  (func $cos (import "Math" "cos") (param f32) (result f32))

  (memory (export "mem") 1)
  ;; длина буфера
  (global $len (mut i32) (i32.const 0))

  ;; увеличиваем память, чтобы в ней
  ;; было минимум len байтов
  (func $resizeMemory
    (local $needBlocks i32)
    (set_local $needBlocks
      (get_global $len)
      (i32.add (i32.const 0xFFFF))
      (i32.shr_u (i32.const 16))
    )

    (if
      (i32.gt_u (get_local $needBlocks) (current_memory))
      (then
        (grow_memory (i32.sub (get_local $needBlocks) (current_memory)))
        (drop)
      )
    )
  )

  ;; запускаем фейерверк
  (func (export "fire") (param $x f32) (param $y f32)
    (local $ptr i32)
    (local $amplitude f32)
    (local $angle f32)

    (loop $each-point
      ;; amplitude = sqrt(random()) * 20
      (set_local $amplitude
        (f32.mul (f32.sqrt (call $random)) (f32.const 20))
      )
      ;; $angle = random() * 2PI
      (set_local $angle
        (f32.mul (call $random) (f32.const 6.283185307179586))
      )

      ;; mem[ptr + 0] = x
      (f32.store offset=0 (get_local $ptr)
        (get_local $x)
      )
      ;; mem[ptr + 4] = y
      (f32.store offset=4 (get_local $ptr)
        (get_local $y)
      )
      ;; mem[ptr + 8] = amplitude * cos(angle)
      (f32.store offset=8 (get_local $ptr)
        (f32.mul
          (get_local $amplitude)
          (call $cos (get_local $angle))
        )
      )
      ;; mem[ptr + 12] = amplitude * sin(angle)
      (f32.store offset=12 (get_local $ptr)
        (f32.mul
          (get_local $amplitude)
          (call $sin (get_local $angle))
        )
      )

      ;; ptr = ptr + 16; _ = ptr
      (tee_local $ptr
        (get_local $ptr)
        (i32.add (i32.const 16))
      )
      ;; if (_ < $len) continue
      (br_if $each-point
        (i32.lt_u
          (get_global $len)
        )
      )
    )
  )

  (func (export "tick") (param $particleCount i32)
    (local $ptr i32)

    ;; len = particleCount * 16
    (set_global $len
      (get_local $particleCount)
      (i32.mul (i32.const 16))
    )
    (call $resizeMemory)

    (loop $each-point
      ;; mem[ptr + 0] += mem[ptr + 8]
      (f32.store offset=0 (get_local $ptr)
        (f32.add
          (f32.load offset=0 (get_local $ptr))
          (f32.load offset=8 (get_local $ptr))
        )
      )
      ;; mem[ptr + 4] += mem[ptr + 12]
      (f32.store offset=4 (get_local $ptr)
        (f32.add
          (f32.load offset=4 (get_local $ptr))
          (f32.load offset=12 (get_local $ptr))
        )
      )
      ;; mem[ptr + 12] = mem[ptr + 12] + 0.25
      (f32.store offset=12 (get_local $ptr)
        (f32.add
          (f32.load offset=12 (get_local $ptr))
          (f32.const 0.25)
        )
      )

      ;; ptr = ptr + 16; _ = ptr
      (tee_local $ptr
        (get_local $ptr)
        (i32.add (i32.const 16))
      )
      ;; if (_ < $len) continue
      (br_if $each-point
        (i32.lt_u
          (get_global $len)
        )
      )
    )
  )
)
