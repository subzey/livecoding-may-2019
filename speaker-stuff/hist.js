const ChildProcess = require('child_process');
const Net = require('net');

function getChildId() {
	const idstr = (ChildProcess
		.execSync(
			`git log --reverse --ancestry-path HEAD..master --format="%H"`
		)
		.toString('utf-8')
		.split(/[\r\n]+/)[0]
	) || '';

	return idstr.trim();
}

function getParentId() {
	return (ChildProcess
		.execSync(
			`git log HEAD~1 --format="%H" -n 1`
		)
		.toString('utf-8')
		.trim()
	);
}

const command = process.argv[2] || '';
let navigateTo;
if (command === 'next') {
	navigateTo = getChildId();
} else if (command === 'prev') {
	navigateTo = getParentId();
} else if (command === '') {
	navigateTo = undefined;
} else {
	console.error(`Unknown command ${command}`);
	process.exit(1)
}

if (navigateTo) {
	ChildProcess.execSync(`git checkout ${navigateTo}`);
}

const rv = {
	now: ChildProcess.execSync(`git show --no-patch --format="%B" HEAD`).toString('utf-8').trim(),
}

const childId = getChildId();

if (childId) {
	rv.next = ChildProcess.execSync(`git show --no-patch --format="%B" ${childId}`).toString('utf-8').trim();
}

const msg = (
	`\x1B[3J\x1B[2J\x1B[H` +
	`\x1B[0;42;30m    Сейчас:    \n`+
	`\x1B[0;1m${rv.now}\n\n` +
	`\x1B[0;44;30m    Далее:     \n`+
	`\x1B[m${rv.next || '---'}`
)

console.log(msg);

// adb forward tcp:2323 tcp:2323
// nc -kl 2323

const client = Net.connect({ port: 2323 }, () => {
	client.write(msg);
	setTimeout(() => client.end(), 1000);
});
client.on('error', e => console.error('\n\x1B[0;31m' + e.message + '\x1B[0m'));
